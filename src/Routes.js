import Home from "Screens/Home";
import Pokedex from "Screens/Pokedex";

export const publicRoutes = [
  {
    component: Home,
    path: "/",
    exact: true,
  },
  {
    component: Pokedex,
    path: "/pokedex",
    exact: true,
  },
];
