import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Layout from "Components/Layout";
class Home extends Component {
  constructor() {
    super();
    this.state = {
      pokemons: [],
      isLoading: true,
      limit: 10,
      offset: 0,
      favorits: [],
    };
  }
  
  componentDidMount() {
    this.fetchdataDenganFetch();
  }
  fetchdataDenganFetch = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => response.json())
      .then((data) =>
        this.setState({ pokemons: data.results, isLoading: false })
      )
      .catch((error) => console.log(error));
  };
  // handlefavorits = (id, name) => {
  //   this.setState({
  //     favorits: this.state.favorits.concat({
  //       id,
  //       name,
  //     }),
  //   });
  // };
  render() {
    console.log(this.props);
    const { pokemons, isLoading, offset } = this.state;
    return (
      <Layout>
        <div className="home">
          <div className="home__title">pokefav</div>
          <div className="home__grid container">
            {this.props.favorit.length === 0 || isLoading
              ? null
              : this.props.favorit.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset === 0 ? index + 1 : offset + 1}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
        <div className="home">
          <div className="home__title">Poke Apps</div>
          <div className="home__grid container">
            {pokemons.length === 0 || isLoading
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={() =>
                          this.props.handlefavorits({
                            newValue: {
                              id: index + offset + 1,
                              name: pokemon.name,
                            },
                          })
                        }
                      >
                        +
                      </button>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset === 0 ? index + 1 : offset + 1}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    favorit: state.favoritPokemon,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handlefavorits: (params) =>
      dispatch({
        type: "SET_FAVORIT_POKEMON",
        newValue: params.newValue,
      }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);