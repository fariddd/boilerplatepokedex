import React from "react";
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Layout from 'Components/Layout'

function Pokedex(props) {
  return (
    <Layout>
    <div className="home">
          <div className="home__title">pokefav</div>
          <div className="home__grid container">
            {props.favorit.length === 0
              ? null
              : props.favorit.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${pokemon.id}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
      </div>
      </Layout>
  )
}

const mapStateToProps = (state) => {
  return {
    favorit: state.favoritPokemon,
  };
};

export default connect(mapStateToProps)(Pokedex);